//
//  PetasenseApiManager.m
//  PetasenseGraph
//
//  Created by Naveen Chaudhary on 09/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import "PetasenseApiManager.h"

static PetasenseApiManager *sharedManager = nil;

@interface PetasenseApiManager ()
@property (nonatomic, strong) NSArray *arrSessionCookies;
@end

@implementation PetasenseApiManager

+ (id)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[PetasenseApiManager alloc] init];
    });
    return sharedManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

#pragma mark - petasense API
- (void)loginWithUsername:(NSString *_Nullable)username andPassword:(NSString *_Nullable)password withCallBack:(void (^_Nullable)(BOOL success, NSDictionary * _Nullable data))callBack {
    
    NSURL *url = [NSURL URLWithString:@"http://ps-staging1.cloudapp.net/api/login"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    NSString *bodyData = [NSString stringWithFormat:@"username=%@&password=%@", username, password];
    request.HTTPMethod = @"POST";
    request.HTTPBody = [bodyData dataUsingEncoding:NSUTF8StringEncoding];
    
    typeof(self) __weak weakSelf = self;
    NSURLSessionDataTask *session = [[NSURLSession sharedSession]
                                     dataTaskWithRequest: request
                                     completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error)
                                     {
                                         NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                         if ([httpResponse statusCode] == 200) {
                                             weakSelf.arrSessionCookies = [NSHTTPCookie cookiesWithResponseHeaderFields:[httpResponse allHeaderFields] forURL:url];
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 callBack (YES, jsonData);
                                             });
                                         } else {
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 callBack (NO, jsonData);
                                             });
                                         }
                                     }];
    [session resume];
}

- (void)getVibrationDataForAxis:(NSString *)axis withCallBack:(void (^)(BOOL, NSDictionary * _Nullable))callBack {
    
    NSString *urlString = [NSString stringWithFormat:@"http://ps-staging1.cloudapp.net/api/webapp/vibration-data/129/broadband-trend?axis=%@", axis];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"GET";
    NSDictionary * headers = [NSHTTPCookie requestHeaderFieldsWithCookies:self.arrSessionCookies];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSessionDataTask *session = [[NSURLSession sharedSession]
                                     dataTaskWithRequest: request
                                     completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error)
                                     {
                                         NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                         if ([httpResponse statusCode] == 200) {
                                             // take out relevant data
                                             NSArray *dataArray = jsonData[@"trend_data"][@"data"];
                                             NSArray *timeArray = jsonData[@"trend_data"][@"time"];
                                             NSMutableArray *dateArray = [NSMutableArray array];
                                             
                                             NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                                             [dateFormat setDateFormat:@"E, dd MMM yyyy HH:mm:ss Z"];
                                             
                                             // convert strings to NSDate
                                             for (NSString *dateString in timeArray) {
                                                 NSDate *date = [dateFormat dateFromString:dateString];
                                                 [dateArray addObject:date];
                                             }
                                             
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 callBack (YES, @{@"data":dataArray, @"time":dateArray});
                                             });
                                         } else {
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 callBack (NO, jsonData);
                                             });
                                         }
                                     }];
    [session resume];
}

- (BOOL)isUserLoggedIn {
    if (self.arrSessionCookies) {
        return YES;
    }
    return NO;
}
@end
