//
//  PetasenseApiManager.h
//  PetasenseGraph
//
//  Created by Naveen Chaudhary on 09/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PetasenseApiManager : NSObject

+ (PetasenseApiManager *_Nullable)sharedInstance;

- (void)loginWithUsername:(NSString *_Nullable)username andPassword:(NSString *_Nullable)password withCallBack:(void (^_Nullable)(BOOL success, NSDictionary * _Nullable data))callBack;

- (void)getVibrationDataForAxis:(NSString *_Nullable)axis withCallBack:(void (^_Nullable)(BOOL success, NSDictionary * _Nullable data))callBack;

- (BOOL)isUserLoggedIn;

@end
