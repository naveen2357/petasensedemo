//
//  ViewController.m
//  PetasenseGraph
//
//  Created by Naveen Chaudhary on 09/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import "ViewController.h"
#import "LoginViewController.h"
#import "PetasenseApiManager.h"
#import "GraphCollectionView.h"
#import "GraphBase.h"
#import "GraphToggleButton.h"
#import "Constants.h"

@interface ViewController ()<GraphCollectionViewDelegate, GraphToggleButtonDelegate>
@property (nonatomic, strong) UILabel *tempLabel;
@property (nonatomic, strong) NSMutableDictionary *dataDict;
@property (nonatomic, weak) GraphCollectionView *graphView;
@property (nonatomic, strong) NSMutableArray *graphKeys;
@property (nonatomic, strong) UIView *readerView;
@property (nonatomic) CGPoint previousReaderLocation;
@property (nonatomic) NSInteger previousDataIndex;

@property (nonatomic, weak) UILabel *titleLabel;
@property (nonatomic, weak) GraphToggleButton *xButton;
@property (nonatomic, weak) GraphToggleButton *yButton;
@property (nonatomic, weak) GraphToggleButton *zButton;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.dataDict = [NSMutableDictionary dictionary];
    self.graphKeys = [NSMutableArray array];
    
    [self createGraph];
    
    [self createGraphReader];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([[PetasenseApiManager sharedInstance] isUserLoggedIn]) {
        [self startDownloadingDataAndCreateGraph];
    } else {
        LoginViewController *loginVC = [[LoginViewController alloc] init];
        [self presentViewController:loginVC animated:NO completion:^{
            
        }];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - view creation
- (void)createGraph {
    [self createLegendAndGraphTitle];
    
    GraphBase *graphBase = [[GraphBase alloc] initWithFrame:CGRectMake(20.0,
                                                                       100.0,
                                                                       self.view.frame.size.width - 60.0,
                                                                       self.view.frame.size.height - 140.0)];
    [self.view addSubview: graphBase];
    
    GraphCollectionView *graphView = [[GraphCollectionView alloc] initWithFrame:CGRectMake(80.0,
                                                                                           100.0,
                                                                                           self.view.frame.size.width - 120.0,
                                                                                           self.view.frame.size.height - 115.0)];
    [graphView setGraphDelegate:self];
    [self.view addSubview:graphView];
    self.graphView = graphView;
}

- (void)createLegendAndGraphTitle {
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 40.0)];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:16.0]];
    [titleLabel setTextColor:[UIColor colorWithWhite:0.3 alpha:1.0]];
    [self.view addSubview:titleLabel];
    self.titleLabel = titleLabel;
    
    GraphToggleButton *xButton = [[GraphToggleButton alloc] initWithFrame:CGRectMake(60.0, 50.0, 150.0, 30.0)];
    [xButton setTitle:@"x-axis" andColor:GRAPH_COLOR_DICT[@"x-axis"]];
    [xButton setDelegate:self];
    [self.view addSubview:xButton];
    self.xButton = xButton;
    
    GraphToggleButton *yButton = [[GraphToggleButton alloc] initWithFrame:CGRectMake(240.0, 50.0, 150.0, 30.0)];
    [yButton setTitle:@"y-axis" andColor:GRAPH_COLOR_DICT[@"y-axis"]];
    [yButton setDelegate:self];
    [self.view addSubview:yButton];
    self.yButton = yButton;
    
    GraphToggleButton *zButton = [[GraphToggleButton alloc] initWithFrame:CGRectMake(420.0, 50.0, 150.0, 30.0)];
    [zButton setTitle:@"z-axis" andColor:GRAPH_COLOR_DICT[@"z-axis"]];
    [zButton setDelegate:self];
    [self.view addSubview:zButton];
    self.zButton = zButton;
}

- (void)reloadGraphs {
    [self.graphView reloadData];
    [self.graphView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:[self.graphView numberOfItemsInSection:0]-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:NO];
}

- (void)createGraphReader {
    UIView *readerView = [[UIView alloc] initWithFrame:CGRectMake(self.graphView.center.x,
                                                                  100.0,
                                                                  24.0,
                                                                  self.graphView.frame.size.height - 30.0)];
    [self.view addSubview:readerView];
    self.readerView = readerView;
    self.previousReaderLocation = readerView.center;
    
    UIView *verticalLine = [[UIView alloc] initWithFrame:CGRectMake(readerView.bounds.size.width/2 - 2.0,
                                                                    0.0,
                                                                    4.0,
                                                                    readerView.bounds.size.height)];
    [verticalLine setBackgroundColor:[UIColor colorWithWhite:0.85 alpha:1.0]];
    [verticalLine.layer setCornerRadius:2.0];
    [readerView addSubview:verticalLine];
    
    UIView *readerGlass = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 24.0, 24.0)];
    [readerGlass setCenter:CGPointMake(readerView.bounds.size.width/2, readerView.bounds.size.height/2)];
    [readerGlass setBackgroundColor:[UIColor colorWithWhite:0.85 alpha:1.0]];
    [readerGlass.layer setCornerRadius:12.0];
    [readerView addSubview:readerGlass];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    [readerGlass addGestureRecognizer:panGesture];
}

- (void)handlePanGesture:(UIPanGestureRecognizer *)gesture {
    CGFloat xPos = MAX(CGRectGetMinX(self.graphView.frame), MIN(CGRectGetMaxX(self.graphView.frame), [gesture locationInView:self.view].x));
    [self.readerView setCenter:CGPointMake(xPos, self.readerView.center.y)];
    
    [self getClosestDataPointAndResetReader];
}

#pragma mark - Network calls
- (void)startDownloadingDataAndCreateGraph {
    [self startDownloadingDataForAxis:@"x"];
    [self startDownloadingDataForAxis:@"y"];
    [self startDownloadingDataForAxis:@"z"];
}

- (void)startDownloadingDataForAxis:(NSString *)axis {
    typeof(self) __weak weakSelf = self;
    
    [[PetasenseApiManager sharedInstance] getVibrationDataForAxis:axis withCallBack:^(BOOL success, NSDictionary * _Nullable data) {
        if (success) {
            NSString *dataKey = [NSString stringWithFormat:@"%@-axis", axis];
            [weakSelf.dataDict setObject:data[@"data"] forKey:dataKey];
            [weakSelf.dataDict setObject:data[@"time"] forKey:@"time"];
            
            if (![weakSelf.graphKeys containsObject:dataKey]) {
                [weakSelf.graphKeys addObject:dataKey];
            }
            [weakSelf reloadGraphs];
            [weakSelf performSelector:@selector(getClosestDataPointAndResetReader) withObject:NULL afterDelay:0.0];
        } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Data Error!" message:data[@"message"] preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }];
}

#pragma mark - GraphCollectionViewDelegate
- (void)getClosestDataPointAndResetReader {
    
    // get current reader position and corresponding cell
    CGPoint graphPoint = CGPointMake(self.graphView.contentOffset.x + self.readerView.center.x - self.graphView.frame.origin.x, self.readerView.bounds.size.height/2);
    NSIndexPath *cellIndexBelowReader = [self.graphView indexPathForItemAtPoint:graphPoint];
    UICollectionViewCell *cell = [self.graphView cellForItemAtIndexPath:cellIndexBelowReader];
    
    // calculate time corrosponding to position
    NSArray *timeArray = [self.dataDict objectForKey:@"time"];
    NSDate *startDate = [self dateAtStartOfDay:[timeArray firstObject]];
    CGFloat timeIntervalInDays = cellIndexBelowReader.item;
    if (cell) {
        timeIntervalInDays += (graphPoint.x - cell.frame.origin.x)/cell.frame.size.width;
    }
    NSDate *currentDate = [startDate dateByAddingTimeInterval:timeIntervalInDays*24*60*60];
    
    // find the closest data point to reader and calculate coordinates for the data point
    NSDate *dataDate = [self getClosestDateInDataToDate:currentDate];
    NSDate *dataStartDate = [self dateAtStartOfDay:dataDate];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:dataStartDate
                                                         options:0];
    
    NSIndexPath *dataIndexPath = [NSIndexPath indexPathForItem:[components day] inSection:0];
    cell = [self.graphView cellForItemAtIndexPath:dataIndexPath];
    
    CGFloat xPos = cell.frame.origin.x + cell.frame.size.width*[dataDate timeIntervalSinceDate:dataStartDate]/(24*60*60) - self.graphView.contentOffset.x + self.graphView.frame.origin.x;
    
    // set reader view to the data point calculated
    if (xPos <= CGRectGetMaxX(self.graphView.frame) && xPos >= CGRectGetMinX(self.graphView.frame)) {
        [self.readerView setCenter:CGPointMake(xPos, self.readerView.center.y)];
        self.previousReaderLocation = self.readerView.center;
        self.previousDataIndex = [timeArray indexOfObject:dataDate];
    } else {
        [self.readerView setCenter:self.previousReaderLocation];
    }
    
    // reload legend and title lable
    [self updateGraphTitleAndLegends];
}

- (NSInteger)getNumberOfCellsToDisplay {
    NSArray *timeArray = [self.dataDict objectForKey:@"time"];
    if (timeArray.count > 0) {
        NSDate *startDate = [self dateAtStartOfDay:[timeArray firstObject]];
        NSDate *endDate = [self dateAtStartOfDay:[timeArray lastObject]];
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                            fromDate:startDate
                                                              toDate:endDate
                                                             options:0];
        return [components day]+1;
    }
    return 0;
}

- (NSDate *)getCellDateForCellAtIndex:(NSInteger)index {
    NSArray *timeArray = [self.dataDict objectForKey:@"time"];
    NSDate *startDate = [self dateAtStartOfDay:[timeArray firstObject]];
    NSDate *currentDate = [startDate dateByAddingTimeInterval:index*24*60*60];
    
    return currentDate;
}

- (NSArray *)getActiveGraphKeys {
    return self.graphKeys;
}

- (NSDictionary *)getDataForCellAtIndex:(NSInteger)index {
    NSArray *timeArray = [self.dataDict objectForKey:@"time"];
    NSDate *startDate = [self dateAtStartOfDay:[timeArray firstObject]];
    NSDate *currentDate = [startDate dateByAddingTimeInterval:index*24*60*60];
    
    return [self extractDataForCellWithDate:currentDate];
}

#pragma mark - private methods
- (void)updateGraphTitleAndLegends {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"MMMM dd, yyyy @ hh:mm a"];
    
    NSString *string = [dateFormatter stringFromDate:[[self.dataDict objectForKey:@"time"] objectAtIndex:self.previousDataIndex]];
    [self.titleLabel setText:string];
    
    [self.xButton updateWithData:[NSString stringWithFormat:@"%.3f", [[[self.dataDict objectForKey:@"x-axis"] objectAtIndex:self.previousDataIndex] floatValue]]];
    [self.yButton updateWithData:[NSString stringWithFormat:@"%.3f", [[[self.dataDict objectForKey:@"y-axis"] objectAtIndex:self.previousDataIndex] floatValue]]];
    [self.zButton updateWithData:[NSString stringWithFormat:@"%.3f", [[[self.dataDict objectForKey:@"z-axis"] objectAtIndex:self.previousDataIndex] floatValue]]];
}

- (NSDate *)getClosestDateInDataToDate:(NSDate *)currentDate
{
    double minDifference = DBL_MAX;
    NSDate *closestDate = nil;
    NSArray *dateArray = [self.dataDict objectForKey:@"time"];
    
    for (NSDate *date in dateArray) {
        if (ABS([date timeIntervalSinceDate:currentDate]) < minDifference) {
            minDifference = ABS([date timeIntervalSinceDate:currentDate]);
            closestDate = date;
        }
    }
    
    return closestDate;
}

// filter data for cell
- (NSDictionary *)extractDataForCellWithDate:(NSDate *)currentDate {
    NSPredicate *lowerPredicate = [NSPredicate predicateWithFormat:@"self < %@", currentDate];
    NSPredicate *higherPredicate = [NSPredicate predicateWithFormat:@"self > %@", [currentDate dateByAddingTimeInterval:24*60*60]];
    NSArray *timeArray = [self.dataDict objectForKey:@"time"];
    NSArray *lowerArray = [timeArray filteredArrayUsingPredicate:lowerPredicate];
    NSArray *higherArray = [timeArray filteredArrayUsingPredicate:higherPredicate];
    
    NSUInteger lowerIndex = 0, higherIndex = timeArray.count-1;
    if (lowerArray.count > 0) {
        lowerIndex = [timeArray indexOfObject:[lowerArray lastObject]];
    }
    if (higherArray.count > 0) {
        higherIndex = [timeArray indexOfObject:[higherArray firstObject]];
    }
    return [self extractDataForCellWithRange:NSMakeRange(lowerIndex, higherIndex-lowerIndex+1)];
}

- (NSDictionary *)extractDataForCellWithRange:(NSRange)range {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    for (NSString *key in self.dataDict.allKeys) {
        [dict setObject:[[self.dataDict objectForKey:key] subarrayWithRange:range] forKey:key];
    }
    return dict;
}

#pragma mark - date helper
- (NSDate *) dateAtStartOfDay:(NSDate *)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:date];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    [components setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    return [[NSCalendar currentCalendar] dateFromComponents:components];
}

#pragma mark - GraphToggleButtonDelegate
- (void)toggleGraphForKey:(NSString *)key {
    if ([self.graphKeys containsObject:key]) {
        [self.graphKeys removeObject:key];
    } else {
        [self.graphKeys addObject:key];
    }
    [self.graphView reloadData];
    [self updateGraphTitleAndLegends];
}
@end
