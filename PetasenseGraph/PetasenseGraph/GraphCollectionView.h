//
//  GraphCollectionView.h
//  PetasenseGraph
//
//  Created by Naveen Chaudhary on 10/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GraphCollectionViewDelegate <NSObject>

- (void)getClosestDataPointAndResetReader;
- (NSInteger)getNumberOfCellsToDisplay;
- (NSArray *)getActiveGraphKeys;
- (NSDate *)getCellDateForCellAtIndex:(NSInteger)index;
- (NSDictionary *)getDataForCellAtIndex:(NSInteger)index;

@end

@interface GraphCollectionView : UICollectionView

@property (nonatomic, weak) id<GraphCollectionViewDelegate> graphDelegate;

@end
