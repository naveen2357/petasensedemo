//
//  GraphCell.h
//  PetasenseGraph
//
//  Created by Naveen Chaudhary on 10/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GraphCell : UICollectionViewCell

- (void)createGraphWithData:(NSDictionary *)dataDictionary andActiveKeys:(NSArray *)arrKeys forDate:(NSDate *)startDate;
- (void)setIsLastCell:(BOOL)isLast;

@end
