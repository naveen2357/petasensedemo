//
//  LoginViewController.m
//  PetasenseGraph
//
//  Created by Naveen Chaudhary on 09/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import "LoginViewController.h"
#import "PetasenseApiManager.h"

@interface LoginViewController ()
@property (nonatomic, weak) UIView *containerView;
@property (nonatomic, weak) UITextField *usernameField;
@property (nonatomic, weak) UITextField *passwordField;
@property (nonatomic, weak) UIButton *loginButton;
@property (nonatomic, weak) UIActivityIndicatorView *activityIndicator;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createSubviews];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    __block CGRect frame = self.containerView.frame;
    frame.origin.y = self.view.frame.size.height/2 - 133.0;
    [UIView animateWithDuration:0.33 animations:^{
        [self.containerView setFrame:frame];
    } completion:^(BOOL finished) {
        frame.size.height = 266.0;
        [self.containerView setFrame:frame];
    }];
}

- (void)createSubviews {
    
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(20.0,
                                                                     self.view.frame.size.height/2 - 20.0,
                                                                     self.view.frame.size.width - 40.0,
                                                                     41.0)];
    [containerView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin];
    [containerView setClipsToBounds:YES];
    [self.view addSubview:containerView];
    self.containerView = containerView;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"petasense-logo"]];
    [imageView setCenter:CGPointMake(containerView.frame.size.width/2, imageView.frame.size.height/2)];
    [imageView setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin];
    [containerView addSubview:imageView];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0,
                                                                    60.0,
                                                                    containerView.frame.size.width,
                                                                    44.0)];
    [titleLabel setText:@"Login to view data"];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:20.0]];
    [titleLabel setTextColor:[UIColor colorWithWhite:0.22 alpha:1.0]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [containerView addSubview:titleLabel];
    
    UIView *usernameContainer = [[UIView alloc] initWithFrame:CGRectMake(0.0,
                                                                         114.0,
                                                                         containerView.frame.size.width,
                                                                         44.0)];
    [usernameContainer setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [usernameContainer.layer setBorderColor:[[UIColor colorWithRed:161.0/255.0 green:163.0/255.0 blue:166.0/255.0 alpha:1.0] CGColor]];
    [usernameContainer.layer setBorderWidth:1.0];
    [usernameContainer.layer setCornerRadius:4.0];
    [containerView addSubview:usernameContainer];
    
    UITextField *usernameField = [[UITextField alloc] initWithFrame:CGRectMake(10.0,
                                                                               0.0,
                                                                               usernameContainer.frame.size.width - 20.0,
                                                                               usernameContainer.frame.size.height)];
    [usernameField setPlaceholder:@"Username"];
    [usernameField setText:@"test@petasense.com"];
    [usernameField setTextColor:[UIColor colorWithWhite:0.22 alpha:1.0]];
    [usernameField setTextContentType:UITextContentTypeEmailAddress];
    [usernameField setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [usernameContainer addSubview:usernameField];
    self.usernameField = usernameField;
    
    UIView *passwordContainer = [[UIView alloc] initWithFrame:CGRectMake(0.0,
                                                                         168.0,
                                                                         containerView.frame.size.width,
                                                                         44.0)];
    [passwordContainer setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [passwordContainer.layer setBorderColor:[[UIColor colorWithRed:161.0/255.0 green:163.0/255.0 blue:166.0/255.0 alpha:1.0] CGColor]];
    [passwordContainer.layer setBorderWidth:1.0];
    [passwordContainer.layer setCornerRadius:4.0];
    [containerView addSubview:passwordContainer];
    
    UITextField *passwordField = [[UITextField alloc] initWithFrame:CGRectMake(10.0,
                                                                               0.0,
                                                                               passwordContainer.frame.size.width - 20.0,
                                                                               passwordContainer.frame.size.height)];
    [passwordField setPlaceholder:@"Password"];
    [passwordField setText:@"pYEMZU3ggJj9u7XD"];
    [passwordField setTextColor:[UIColor colorWithWhite:0.22 alpha:1.0]];
    [passwordField setSecureTextEntry:YES];
    [passwordField setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [passwordContainer addSubview:passwordField];
    self.passwordField = passwordField;
    
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginButton setFrame:CGRectMake(0.0,
                                     222.0,
                                     containerView.frame.size.width,
                                     44.0)];
    [loginButton setTitle:@"Login" forState:UIControlStateNormal];
    [loginButton setBackgroundColor:[UIColor colorWithRed:175.0/255.0 green:196.0/255.0 blue:81.0/255.0 alpha:1.0]];
    [loginButton addTarget:self action:@selector(loginButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [loginButton setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [loginButton.layer setCornerRadius:4.0];
    [containerView addSubview:loginButton];
    self.loginButton = loginButton;
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    activityIndicator.center = CGPointMake(loginButton.frame.size.width/2, loginButton.frame.size.height/2);
    [activityIndicator setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin];
    [loginButton addSubview: activityIndicator];
    self.activityIndicator = activityIndicator;
}

#pragma mark - user action
- (void)loginButtonClicked:(id)sender {
    [self.loginButton setTitle:@"" forState:UIControlStateNormal];
    [self.activityIndicator startAnimating];
    
    typeof(self) __weak weakSelf = self;

    [[PetasenseApiManager sharedInstance] loginWithUsername:self.usernameField.text andPassword:self.passwordField.text withCallBack:^(BOOL success, NSDictionary * _Nullable data) {
        [weakSelf.loginButton setTitle:@"Login" forState:UIControlStateNormal];
        [weakSelf.activityIndicator stopAnimating];
        if (success) {
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
        } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Login Error!" message:data[@"message"] preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }];
}

#pragma mark - keyboard notifications
-(void) keyboardWillShow:(NSNotification*) notification {
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    double duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];

    CGRect containerFrame = self.containerView.frame;
    containerFrame.origin.y = self.view.frame.size.height - keyboardFrame.size.height - containerFrame.size.height - 5.0;
    
    [UIView animateWithDuration:duration animations:^{
        [self.containerView setFrame:containerFrame];
    }];
}

-(void) keyboardWillHide:(NSNotification*) notification {
    double duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];

    CGRect containerFrame = self.containerView.frame;
    containerFrame.origin.y = self.view.frame.size.height/2 - 138.0;

    [UIView animateWithDuration:duration animations:^{
        [self.containerView setFrame:containerFrame];
    }];
}
@end
