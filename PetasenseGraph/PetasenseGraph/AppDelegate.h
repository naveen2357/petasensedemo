//
//  AppDelegate.h
//  PetasenseGraph
//
//  Created by Naveen Chaudhary on 09/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

