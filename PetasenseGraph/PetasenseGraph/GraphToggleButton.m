//
//  GraphToggleButton.m
//  PetasenseGraph
//
//  Created by Naveen Chaudhary on 10/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import "GraphToggleButton.h"

@interface GraphToggleButton ()

@property (nonatomic, weak) UILabel *titleLabel;
@property (nonatomic, weak) UIView *circle;
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, strong) NSString *keyString;
@property (nonatomic) BOOL isOn;

@end

@implementation GraphToggleButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UIView *circle = [[UIView alloc] initWithFrame:CGRectMake(10.0, 10.0, 10.0, 10.0)];
        [circle.layer setCornerRadius:5.0];
        [self addSubview:circle];
        self.circle = circle;
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(30.0, 0.0, self.frame.size.width-40.0, self.frame.size.height)];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setTextColor:[UIColor colorWithWhite:0.45 alpha:1.0]];
        [titleLabel setFont:[UIFont boldSystemFontOfSize:14.0]];
        [self addSubview:titleLabel];
        self.titleLabel = titleLabel;
        
        [self.layer setBorderColor:[[UIColor colorWithWhite:0.95 alpha:1.0] CGColor]];
        [self.layer setBorderWidth:1.0];
        [self.layer setCornerRadius:4.0];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}

- (void)setTitle:(NSString *)title andColor:(UIColor *)color {
    self.isOn = YES;
    self.color = color;
    self.keyString = title;
    [self.circle setBackgroundColor:color];
    [self.titleLabel setText:title];
}

- (void)updateWithData:(NSString *)data {
    if (self.isOn) {
        [self.titleLabel setText:[NSString stringWithFormat:@"%@ - %@", self.keyString, data]];
    } else {
        [self.titleLabel setText:self.keyString];
    }
}

- (void)handleTapGesture:(id)sender {
    if ([self.delegate respondsToSelector:@selector(toggleGraphForKey:)]) {
        self.isOn = !self.isOn;
        if (self.isOn) {
            [self.circle setBackgroundColor:self.color];
        } else {
            [self.circle setBackgroundColor:[UIColor clearColor]];
        }
        [self.delegate toggleGraphForKey:self.keyString];
    }
}

@end
