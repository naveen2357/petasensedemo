//
//  GraphBase.m
//  PetasenseGraph
//
//  Created by Naveen Chaudhary on 10/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import "GraphBase.h"

@implementation GraphBase

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createGrids];
    }
    return self;
}

- (void)createGrids {
    UIBezierPath *path = [[UIBezierPath alloc] init];

    // horizontal grids
    for (NSUInteger row=0; row<7; row++) {
        [path moveToPoint:CGPointMake(50.0, row*((self.frame.size.height-20.0)/6)+10.0)];
        [path addLineToPoint:CGPointMake(self.frame.size.width+10.0, row*((self.frame.size.height-20.0)/6)+10.0)];
        [self createLabelWithText:[NSString stringWithFormat:@"%.1f",(1.5 - row*0.25)] atPosition:(row*((self.frame.size.height-20.0)/6)+10.0)];
    }
    
    // 2 vertical grids at the ends
    [path moveToPoint:CGPointMake(60.0, 0.0)];
    [path addLineToPoint:CGPointMake(60.0, self.frame.size.height + 25.0)];
    [path moveToPoint:CGPointMake(self.frame.size.width, 0.0)];
    [path addLineToPoint:CGPointMake(self.frame.size.width, self.frame.size.height+25.0)];
    
    CAShapeLayer *lineLayer = [CAShapeLayer layer];
    [lineLayer setFrame:self.bounds];
    [lineLayer setPath:[path CGPath]];
    [lineLayer setStrokeColor:[[UIColor colorWithWhite:0.95 alpha:1.0] CGColor]];
    [lineLayer setLineWidth:1.0];
    [self.layer addSublayer:lineLayer];
}

// create y-axis labels
- (void)createLabelWithText:(NSString *)text atPosition:(CGFloat)yPos {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.0,
                                                               yPos - 10.0,
                                                               50.0,
                                                               20.0)];
    [label setText:text];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor colorWithWhite:0.4 alpha:1.0]];
    [label setFont:[UIFont systemFontOfSize:12.0]];
    [self addSubview:label];
}
@end
