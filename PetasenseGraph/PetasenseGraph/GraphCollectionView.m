//
//  GraphCollectionView.m
//  PetasenseGraph
//
//  Created by Naveen Chaudhary on 10/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import "GraphCollectionView.h"
#import "GraphCell.h"

@interface GraphCollectionView ()<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic) CGFloat graphScale;

@end

@implementation GraphCollectionView

- (instancetype)initWithFrame:(CGRect)frame {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    self = [super initWithFrame: frame collectionViewLayout:layout];
    if (self) {
        self.graphScale = 1.0;
        
        [self setBackgroundColor:[UIColor clearColor]];
        [self setDelegate:self];
        [self setDataSource:self];
        [self setShowsHorizontalScrollIndicator:NO];
        
        [self registerClass:[GraphCell class] forCellWithReuseIdentifier:@"GraphCell"];
        
        
        UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
        [self addGestureRecognizer:pinchGesture];
        
        UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTapGesture:)];
        [doubleTapGesture setNumberOfTapsRequired:2];
        [self addGestureRecognizer:doubleTapGesture];
    }
    return self;
}

- (void)handlePinchGesture:(UIPinchGestureRecognizer *)gesture
{
    static CGFloat initialScale;
    static CGFloat initialOffset;
    static NSIndexPath *indexPathOfCenterCell;
    
    if (gesture.state == UIGestureRecognizerStateBegan) {
        initialScale = self.graphScale;
        initialOffset = self.contentOffset.x;
        indexPathOfCenterCell = [self indexPathForItemAtPoint:CGPointMake(initialOffset + self.bounds.size.width/2,
                                                                          self.bounds.size.height/2)];
    }
    else if (gesture.state == UIGestureRecognizerStateChanged) {
        CGFloat scale = initialScale * gesture.scale;
        if (scale != self.graphScale && scale >= 1.0 && scale <= 4.0) {
            self.graphScale = MIN(MAX(1.0, scale), 4.0);
            
            [self reloadData];
            
            // reset offset
            [self scrollToItemAtIndexPath:indexPathOfCenterCell atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
        }
    }
}

- (void)handleDoubleTapGesture:(UITapGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateEnded) {
        NSIndexPath *indexPathOfCenterCell = [self indexPathForItemAtPoint:[gesture locationInView:self]];
        
        if (self.graphScale < 4.0) {
            self.graphScale = 4.0;
        } else {
            self.graphScale = 1.0;
        }
        
        [self reloadData];
        [self scrollToItemAtIndexPath:indexPathOfCenterCell atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    }
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.graphDelegate getNumberOfCellsToDisplay];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *cellDict = [self.graphDelegate getDataForCellAtIndex:indexPath.item];
    NSArray *activeKeys = [self.graphDelegate getActiveGraphKeys];
    NSDate *cellDate = [self.graphDelegate getCellDateForCellAtIndex:indexPath.item];
    
    GraphCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GraphCell" forIndexPath:indexPath];
    [cell createGraphWithData:cellDict andActiveKeys:activeKeys forDate:cellDate];
    [cell setIsLastCell:(indexPath.item == [collectionView numberOfItemsInSection:0]-1)];
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(75.0*self.graphScale, collectionView.frame.size.height);
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([self.graphDelegate respondsToSelector:@selector(getClosestDataPointAndResetReader)]) {
        [self.graphDelegate getClosestDataPointAndResetReader];
    }
}

@end
