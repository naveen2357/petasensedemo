//
//  Constants.h
//  PetasenseGraph
//
//  Created by Naveen Chaudhary on 10/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define GRAPH_COLOR_DICT @{@"x-axis" : [UIColor colorWithRed:52.0/255.0 green:152.0/255.0 blue:219.0/255.0 alpha:1.0], @"y-axis" : [UIColor colorWithRed:251.0/255.0 green:225.0/255.0 blue:50.0/255.0 alpha:1.0], @"z-axis" : [UIColor colorWithRed:174.0/255.0 green:194.0/255.0 blue:80.0/255.0 alpha:1.0]}

#endif /* Constants_h */
