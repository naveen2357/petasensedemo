//
//  GraphCell.m
//  PetasenseGraph
//
//  Created by Naveen Chaudhary on 10/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import "GraphCell.h"
#import "Constants.h"

@interface GraphCell ()

@property (nonatomic, strong) NSMutableArray *graphArray;
@property (nonatomic, weak) UILabel *dateLabel;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, weak) UIView *rightBorder;

@end

@implementation GraphCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.graphArray = [NSMutableArray array];
        [self setClipsToBounds:YES];
        [self createCellSubviews];
    }
    return self;
}

#pragma mark - private methods
- (void)createCellSubviews {
    // create right border that will act as grid line
    UIView *rightBorder = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width-1.0, 0.0, 1.0, self.frame.size.height)];
    [rightBorder setBackgroundColor:[UIColor colorWithWhite:0.95 alpha:1.0]];
    [rightBorder setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight];
    [self addSubview:rightBorder];
    self.rightBorder = rightBorder;
    
    // x-axis label for graph
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0,
                                                                   self.contentView.frame.size.height - 30.0,
                                                                   self.contentView.frame.size.width,
                                                                   30.0)];
    [dateLabel setTextAlignment:NSTextAlignmentCenter];
    [dateLabel setTextColor:[UIColor colorWithWhite:0.4 alpha:1.0]];
    [dateLabel setFont:[UIFont systemFontOfSize:12.0]];
    [dateLabel setNumberOfLines:0];
    [dateLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [self.contentView addSubview:dateLabel];
    self.dateLabel = dateLabel;
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@" MMM dd \n yyyy "];
}

- (void)addGraphWithData:(NSArray *)arrData andTimeInfo:(NSArray *)arrTime forDate:(NSDate *)startDate andColor:(UIColor *)color {
    
    CAShapeLayer *graphLayer = [CAShapeLayer layer];
    [graphLayer setFrame:self.contentView.bounds];
    [graphLayer setStrokeColor:[color CGColor]];
    [graphLayer setLineWidth:4.0];
    [graphLayer setFillColor:[[UIColor clearColor] CGColor]];
    [self.layer addSublayer:graphLayer];
    [self.graphArray addObject:graphLayer];
    
    UIBezierPath *path = [[UIBezierPath alloc] init];
    
    for (NSUInteger i=0; i<arrData.count; i++) {
        CGFloat data = [[arrData objectAtIndex:i] floatValue];
        NSDate *date = [arrTime objectAtIndex:i];
        
        CGFloat yPos = self.frame.size.height - 35.0 - (self.frame.size.height-45.0)*data/1.5;
        CGFloat xPos = self.frame.size.width*[date timeIntervalSinceDate:startDate]/(24*60*60);
        
        if (i == 0) {
            [path moveToPoint:CGPointMake(xPos, yPos)];
        } else {
            [path addLineToPoint:CGPointMake(xPos, yPos)];
        }
    }
    
    [graphLayer setPath:[path CGPath]];
    
    
    [self.dateLabel setText:[self.dateFormatter stringFromDate:startDate]];
}


- (void)removeAllGraphs {
    for (CAShapeLayer *graph in self.graphArray) {
        [graph removeFromSuperlayer];
    }
    self.graphArray = [NSMutableArray array];
}

#pragma mark - public methods
- (void)setIsLastCell:(BOOL)isLast {
    [self.rightBorder setHidden:isLast];
}

- (void)createGraphWithData:(NSDictionary *)dataDictionary andActiveKeys:(NSArray *)arrKeys forDate:(NSDate *)startDate {
    // clear the cell before redrawing
    [self removeAllGraphs];
    
    for (NSString *key in arrKeys) {
        [self addGraphWithData:dataDictionary[key] andTimeInfo:dataDictionary[@"time"] forDate:startDate andColor:GRAPH_COLOR_DICT[key]];
    }
}

@end
