//
//  GraphToggleButton.h
//  PetasenseGraph
//
//  Created by Naveen Chaudhary on 10/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GraphToggleButtonDelegate <NSObject>

-(void)toggleGraphForKey:(NSString *)key;

@end

@interface GraphToggleButton : UIView

- (void)setTitle:(NSString *)title andColor:(UIColor *)color;
- (void)updateWithData:(NSString *)data;
@property (nonatomic, weak) id<GraphToggleButtonDelegate> delegate;

@end
